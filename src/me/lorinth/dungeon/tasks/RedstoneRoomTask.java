package me.lorinth.dungeon.tasks;

import java.util.ArrayList;

import me.lorinth.dungeon.objects.CombatRoom;
import me.lorinth.dungeon.objects.RedstoneRoom;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;

public class RedstoneRoomTask extends BukkitRunnable{

	RedstoneRoom r;
	
	public RedstoneRoomTask(RedstoneRoom r){
		this.r = r;
	}

	@Override
	public void run() {
		boolean complete = true;
		for(Location l : r.powered_blocks){					
			if(l.getBlock().getBlockPower() == 0){
				complete = false;
				break;
			}
		}
		
		if(complete){
			r.complete();
			this.cancel();
		}
	}
	
}
