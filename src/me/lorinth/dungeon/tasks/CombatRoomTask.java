package me.lorinth.dungeon.tasks;

import java.util.ArrayList;

import me.lorinth.dungeon.objects.CombatRoom;

import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;

public class CombatRoomTask extends BukkitRunnable{

	CombatRoom r;
	
	public CombatRoomTask(CombatRoom r){
		this.r = r;
	}

	@Override
	public void run() {
		boolean cleared = true;
		for(Entity e : r.mobs){
			if(!e.isDead()){
				cleared = false;
				break;
			}
		}
		
		if(cleared){
			r.mobs = new ArrayList<Entity>();
			r.complete();
			this.cancel();
		}
	}
	
}
