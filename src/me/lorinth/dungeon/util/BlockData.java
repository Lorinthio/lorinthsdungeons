package me.lorinth.dungeon.util;

import org.bukkit.Material;

public class BlockData{
	
	public Material material;
	public byte data;
	
	public BlockData(Material mat, byte data){
		this.material = mat;
		this.data = data;
	}
	
	@Override
	public String toString(){
		return material.getId() + " : " + data;
	}
	
}
