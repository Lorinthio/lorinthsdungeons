package me.lorinth.dungeon.util;

public class Vector3 {
	
	//Simple object storing a coordinate in 3D space
	
	public static final Vector3 EMPTY = new Vector3(0, 0, 0);
	
	public int x;
	public int y;
	public int z;
	
	public Vector3(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	@Override
	public String toString(){
		return "<" + x + "," + y + "," + z + ">";
	}
	
}
