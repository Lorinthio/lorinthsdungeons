package me.lorinth.dungeon.util;

import java.util.ArrayList;
import java.util.HashMap;

import me.lorinth.dungeon.objects.Room.DoorState;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

public class Region {

	//A class which stores a snapshot of before/after changes have been made to it
	//Calculates changes between two vector3's
	
	private HashMap<Location, BlockData> before;
	private HashMap<Location, BlockData> after;
	private ArrayList<Location> changed;
	private Vector3 min;
	private Vector3 max;
	
	public Vector3 a = Vector3.EMPTY; //Used simply for selecting the region initially
	public Vector3 b = Vector3.EMPTY; //"    "
	
	
	private World world;
	
	public Region(World w){
		this.world = w;
	}
	
	public void resolveMaxMin(){
		this.min = new Vector3(Math.min(a.x, b.x), Math.min(a.y, b.y), Math.min(a.z, b.z));
		this.max = new Vector3(Math.max(a.x, b.x), Math.max(a.y, b.y), Math.max(a.z, b.z));
	}
	
	//Handles left click region selection for doors
	public void handleRegionSelectionA(Vector3 a){
		if(!a.equals(Vector3.EMPTY))
			this.a = a;
	}
	
	//Handles right click region selection for doors
	public void handleRegionSelectionB(Vector3 b){
		if(!b.equals(Vector3.EMPTY))
			this.b = b;
	}
	
	public void copyBefore(){
		before = new HashMap<Location, BlockData>();
		
		for(int x=min.x; x<=max.x; x++){
			for(int y=min.y; y<=max.y; y++){
				for(int z=min.z; z<=max.z; z++){
					Block b = world.getBlockAt(x, y, z);
					before.put(b.getLocation(), new BlockData(b.getType(), b.getData()));
				}
			}
		}
	}
	
	public void copyAfter(){
		after = new HashMap<Location, BlockData>();
		
		for(int x=min.x; x<=max.x; x++){
			for(int y=min.y; y<=max.y; y++){
				for(int z=min.z; z<=max.z; z++){
					Block b = world.getBlockAt(x, y, z);
					after.put(b.getLocation(), new BlockData(b.getType(), b.getData()));
				}
			}
		}
	}
	
	public HashMap<DoorState, HashMap<Location, BlockData>> getResolvedDoors()
	{
		resolveChanges();
		HashMap<DoorState, HashMap<Location, BlockData>> doors = new HashMap<DoorState, HashMap<Location, BlockData>>();
		doors.put(DoorState.Open, after);
		doors.put(DoorState.Closed, before);
		return doors;
	}
	
	public void resolveChanges(){
		//Empty hash for storing all the changed blocks
		changed = new ArrayList<Location>(); //flags which locations to remember
		ArrayList<Location> not_changed = new ArrayList<Location>(); //flags which locations to ignore 
		
		//Iterate through all the blocks in the region selection from min to max
		for(int x=min.x; x<=max.x; x++){
			for(int y=min.y; y<=max.y; y++){
				for(int z=min.z; z<=max.z; z++){
					Location l = world.getBlockAt(x, y, z).getLocation();
					BlockData before = this.before.get(l);
					BlockData after = this.after.get(l);
					
					if(before.material == after.material && before.data == after.data){
						not_changed.add(l);
					}
					else{
						changed.add(l);
					}
				}
			}
		}
		
		//For all the blocks that stay the same we want to ignore these
		for(Location l : not_changed){
			before.remove(l);
			after.remove(l);
		}
		
		//System.out.println(before.values().toString());
	}
	
}
