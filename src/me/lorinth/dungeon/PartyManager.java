package me.lorinth.dungeon;

import java.util.ArrayList;
import java.util.HashMap;

import me.lorinth.dungeon.objects.Party;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class PartyManager {

	public HashMap<Player, Party> player_parties = new HashMap<Player, Party>();
	public HashMap<Player, Party> party_invites = new HashMap<Player, Party>();
	public ArrayList<Party> parties = new ArrayList<Party>();
	static PartyManager self;
	
	public PartyManager(){
		self = this;
	}
	
	public void handleCommand(Player p, String[] args){
		if(args.length >= 1){
			String command = args[0];
			if(command.equalsIgnoreCase("create")){
				player_parties.put(p, new Party(p));
			}
			else if(command.equalsIgnoreCase("invite")){
				if(args.length >= 2){
					Player target = Bukkit.getPlayer(args[1]);
					if(target != null){
						if(playerHasParty(p)){
							if(playerHasParty(target)){
								this.sendPartyMessage(p, ChatColor.GOLD + target.getDisplayName() + ChatColor.RED + " is already in a party!");
							}
							else{
								Party party = getPartyOfPlayer(p);
								party_invites.put(target, party);
								sendPartyMessage(p, "You have invited, " + ChatColor.GOLD + target.getDisplayName());
								sendPartyMessage(target, "You have been invited to, " + ChatColor.GOLD + p.getDisplayName() + ChatColor.DARK_AQUA + "'s party!");
							}
						}
					}
					else{
						sendPartyMessage(p, ChatColor.RED + "Couldn't find a user based on the tag, " + ChatColor.GOLD + args[1]);
					}
				}
				else{
					sendCommandMessage(p, "invite", "player", "invites a player to your party, if you do not have a party it will create one");
				}
			}
			else if(command.equalsIgnoreCase("leave")){
				if(playerHasParty(p)){
					player_parties.get(p).removePlayer(p);
				}
			}
			else if(command.equalsIgnoreCase("disband")){
				if(playerHasParty(p)){
					Party party = getPartyOfPlayer(p);
					if(party.isLeader(p)){
						party.disband();
					}
				}
			}
			else if(command.equalsIgnoreCase("promote")){
				Player target = Bukkit.getPlayer(args[1]);
				if(playerHasParty(p)){
					Party party = getPartyOfPlayer(p);
					if(party.isLeader(p) && party.isMember(target)){
						party.changeLeader(target);
					}
				}
			}
			else if(command.equalsIgnoreCase("join")){
				if(party_invites.containsKey(p)){
					party_invites.get(p).addPlayer(p);
					party_invites.remove(p);
				}
			}
			else if(command.equalsIgnoreCase("help")){
				displayHelp(p);
			}
			else{
				displayHelp(p);
			}
		}
		else{
			displayHelp(p);
		}
	}
	
	public void onPlayerJoin(PlayerQuitEvent e){
		if(this.playerHasParty(e.getPlayer())){
			player_parties.get(e.getPlayer()).removePlayer(e.getPlayer());
		}
	}
	
	//Sends all help messages to a player
	public void displayHelp(Player p){
		p.sendMessage(ChatColor.DARK_AQUA + "Party Commands");
		sendCommandMessage(p, "create", (String) null, "creates a party");
		sendCommandMessage(p, "disband", (String) null, "disbands your party");
		sendCommandMessage(p, "invite", "player", "invites a player to your party, if you do not have a party it will create one");
		sendCommandMessage(p, "join", (String) null, "joins the most recent party invite");
		sendCommandMessage(p, "leave", (String) null, "leave your current party! If you are the leader it will be given to the next person that was invited");
		sendCommandMessage(p, "promote", "player", "promotes a player in your party to the current leader");
	}
	
	//Used for sending command syntax to a player with a description
	private void sendCommandMessage(Player reciever, String command, String arg, String description){
		String line = ChatColor.DARK_AQUA + "/party " + command + " ";
		if(arg != null){
			line += "<" + arg + "> ";
		}
		line += ChatColor.GOLD + "- " + description;
		reciever.sendMessage(line);
	}
	
	//Currently not used but created in case we have multi-arg commands
	private void sendCommandMessage(Player reciever, String command, String[] args, String description){
		String line = ChatColor.DARK_AQUA + "/party " + command + " ";
		if(args != null){
			for(String s : args){
				line += "<" + s + "> ";
			}
		}
		line += ChatColor.GOLD + "- " + description;
		reciever.sendMessage(line);
	}
	
	public static PartyManager getPartyManager(){
		return self;
	}
	
	//Check if a player has a party
	public boolean playerHasParty(Player p){
		return player_parties.containsKey(p);
	}
	
	//Get players party, otherwise throw nullpointer for easier handling
	//Be sure to use playerHasParty() first
	public Party getPartyOfPlayer(Player p) throws NullPointerException{
		if(playerHasParty(p)){
			return player_parties.get(p);
		}
		else{
			throw new NullPointerException();
		}
	}
	
	public void sendPartyMessage(Player p, String m){
		p.sendMessage(ChatColor.DARK_AQUA + "[" + ChatColor.GOLD + "Party" + ChatColor.DARK_AQUA + "]:" + m);
	}
	
}
