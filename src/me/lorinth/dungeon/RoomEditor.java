package me.lorinth.dungeon;

import java.util.ArrayList;

import me.Lorinth.BossApi.BossApi;
import me.lorinth.dungeon.objects.CombatRoom;
import me.lorinth.dungeon.objects.Dungeon;
import me.lorinth.dungeon.objects.RedstoneRoom;
import me.lorinth.dungeon.objects.Room;
import me.lorinth.dungeon.objects.Room.RoomType;
import me.lorinth.dungeon.util.Region;
import me.lorinth.dungeon.util.Vector3;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class RoomEditor implements Listener{

	DungeonMain main;
	Player p;
	Dungeon d;
	Room currentRoom;
	RoomType currentRoomType;
	RoomCreationState state = RoomCreationState.Basic;
	
	Region currentDoorRegion;
	
	public enum RoomCreationState{
		Basic, EntryDoorRegion, EntryDoorOpen, EntryDoorClosed, ExitDoorRegion, ExitDoorOpen, ExitDoorClosed, SpawnPlacement, RedstoneSelection
	}
	
	public RoomEditor(DungeonMain main, Player p, Dungeon d){
		this.main = main;
		this.p = p;
		this.d = d;
		
		Bukkit.getPluginManager().registerEvents(this, DungeonMain.getInstance());
		
		sendMessage(p, "Welcome to the room editor!!!");
		sendMessage(p, "To get started use the command " + ChatColor.GOLD + "/room" + ChatColor.GRAY + ", or " + ChatColor.GOLD + "/room help");
	}
	
	public void finish(){
		HandlerList.unregisterAll(this);
	}
	
	public void handleCommand(String[] args){
		if(args.length > 0){
			String cmd = args[0];
			if(cmd.equalsIgnoreCase("exit")){
				this.main.roomEditors.remove(p);
				this.main.sendDungeonMessage(p, "You have exited the room editor.");
				this.finish();
			}
			else if(cmd.equalsIgnoreCase("delete")){
				int pos = Integer.parseInt(args[1]);
				pos -= 1;
				d.rooms.remove(pos);
				pos += 1;
				sendMessage(p, "You haved delete room number, " + pos);
			}
			else if(cmd.equalsIgnoreCase("next")){
				if(state == RoomCreationState.EntryDoorRegion){
					currentDoorRegion.resolveMaxMin();
					sendMessage(p, "Now make changes to the entry door so it is in the closed state");
					sendMessage(p, "When you are finished use the command, " + ChatColor.GOLD + "/room next");
					state = RoomCreationState.EntryDoorClosed;
				}
				else if(state == RoomCreationState.EntryDoorClosed){
					currentDoorRegion.copyBefore();
					
					sendMessage(p, "Now make changes to the entry door so it is open");
					sendMessage(p, "When you are finished use the command, " + ChatColor.GOLD + "/room next");
					state = RoomCreationState.EntryDoorOpen;
				}
				else if(state == RoomCreationState.EntryDoorOpen){
					currentDoorRegion.copyAfter();
					this.currentRoom.entrance = currentDoorRegion.getResolvedDoors();
					
					sendMessage(p, "Now select the region for the exit door");
					state = RoomCreationState.ExitDoorRegion;
					currentDoorRegion = new Region(p.getWorld());
				}
				else if(state == RoomCreationState.ExitDoorRegion){
					currentDoorRegion.resolveMaxMin();
					sendMessage(p, "Now make changes to the exit door so it is in the closed state");
					sendMessage(p, "When you are finished use the command, " + ChatColor.GOLD + "/room next");
					state = RoomCreationState.ExitDoorClosed;
				}
				else if(state == RoomCreationState.ExitDoorClosed){
					currentDoorRegion.copyBefore();
					sendMessage(p, "Now edit the door for its opened state");
					sendMessage(p, "When you are finished use the command, " + ChatColor.GOLD + "/room next");
					state = RoomCreationState.ExitDoorOpen;
				}
				else if(currentRoomType == RoomType.Combat && state == RoomCreationState.ExitDoorOpen){
					currentDoorRegion.copyAfter();
					this.currentRoom.exit = currentDoorRegion.getResolvedDoors();
					//sendMessage(p, "Select locations for mob spawns with creature/boss name");
					state = RoomCreationState.SpawnPlacement;
					sendMessage(p, "Stand in a monster spawn location and use the following command...");
					sendMessage(p, ChatColor.GOLD + "/room setspawn <mobtype> <count>");
				}
				else if(currentRoomType == RoomType.Redstone && state == RoomCreationState.ExitDoorOpen){
					sendMessage(p, "Select the block(s) that needs to be powered to finish the room");
					state = RoomCreationState.RedstoneSelection;
				}
				else if(state == RoomCreationState.SpawnPlacement || state == RoomCreationState.RedstoneSelection){
					currentRoom = null;
					state = RoomCreationState.Basic;
					sendMessage(p, "You have finished editing the room!");
				}
			}
			else if(cmd.equalsIgnoreCase("create")){
				boolean hasInsert = false;
				int pos = 0;
				if(args.length == 3){
					pos = Integer.parseInt(args[2]) - 1; //makes this user friendly
					hasInsert = true;
				}
				
				RoomType type = RoomType.valueOf(args[1]);
				if(type == RoomType.Combat){
					CombatRoom r = new CombatRoom(d);
					if(hasInsert){
						d.rooms.add(pos, r);
						sendMessage(p, "The combat room(" + pos + ") was created/inserted");
					}
					else{
						d.rooms.add(r);
						sendMessage(p, "The combat room(" + d.rooms.size() + ") was created");
					}
					this.currentRoomType = RoomType.Combat;
					this.state = RoomCreationState.EntryDoorRegion;
					this.currentDoorRegion = new Region(p.getWorld());
					sendMessage(p, "The entrance/exit doors to the room should be in the closed state right now.");
					sendMessage(p, "Please select the two ends of the entry door edit region, once the doors are closed as they should be and the region for the door selected...");
					sendMessage(p, "use the command, " + ChatColor.GOLD + "/room next");
					this.currentRoom = r;
				}
				else if(type == RoomType.Redstone){
					RedstoneRoom r = new RedstoneRoom(d);
					if(hasInsert){
						d.rooms.add(pos, r);
						sendMessage(p, "The redstone room(" + pos + ") was created/inserted");
					}
					else{
						d.rooms.add(r);
						sendMessage(p, "The redstone room(" + d.rooms.size() + ") was created");
					}
					this.currentRoomType = RoomType.Redstone;
					this.state = RoomCreationState.EntryDoorRegion;
					this.currentDoorRegion = new Region(p.getWorld());
					this.currentRoom = r;
					sendMessage(p, "The entrance/exit doors to the room should be in the closed state right now.");
					sendMessage(p, "Please select two ends of the entry door edit region using " + ChatColor.GOLD + "left" + ChatColor.GRAY + "/" + ChatColor.GOLD + "right click" + ChatColor.GRAY + "! Once the region for the door is selected use the command, " + ChatColor.GOLD + "/room next");
				}
				else{
					sendMessage(p, "Invalid room type, " + args[1] + ", use either " + ChatColor.GOLD + "Combat" + ChatColor.GRAY +" or " + ChatColor.GOLD + "Redstone");
				}
			}
			
			// /room animate <pos> <door/loop>
			else if(cmd.equalsIgnoreCase("animate")){
				if(args.length < 3){
					sendMessage(p, ChatColor.RED + "USAGE : " + ChatColor.GRAY + "/room animate <pos> <door/loop>");
				}
				Integer pos = Integer.parseInt(args[1]);
				String type = args[2];
				if(type.equalsIgnoreCase("door")){
					//TODO make the door animation seq
				}
				else if(type.equalsIgnoreCase("loop")){
					//TODO make loop animation seq
				}
				else{
					sendMessage(p, "Invalid animation type, " + args[2]);
				}
			}
			else if(cmd.equalsIgnoreCase("setspawn")){
				if(checkEntityName(args[1])){
					Location loc = p.getLocation().clone();
					ArrayList<String> mobs = new ArrayList<String>();
					int count = Integer.parseInt(args[2]);
					
					CombatRoom r = (CombatRoom) this.currentRoom;
					
					if(isBoss(args[1].trim())){
						for(int i=0; i<count; i++){
							mobs.add(args[1]);
						}
						r.boss_spawns.put(loc, mobs);
						sendMessage(p, "Added " + count + " boss spawn point(s) for " + args[1]);
					}
					else{
						for(int i=0; i<count; i++){
							mobs.add(args[1].toUpperCase());
						}
						r.normal_spawns.put(loc, mobs);
						sendMessage(p, "Added " + count + " " + args[1] + " spawn point(s)");
					}
				}
				else{
					sendMessage(p, "The type, " + args[1] + ", is not a valid mobtype");
				}
			}
			else if(cmd.equalsIgnoreCase("help")){
				sendHelp(p);
			}
			else if(cmd.equalsIgnoreCase("list")){
				Integer index = 1;
				for(Room r : d.rooms){
					String message = "[" + index + "] : ";
					if(r.type == RoomType.Combat){
						message += ChatColor.RED + "Combat";
					}
					else if(r.type == RoomType.Redstone){
						message += ChatColor.GOLD + "Redstone";
					}
					sendMessage(p, message);
					index += 1;
				}
			}
			else if(cmd.equalsIgnoreCase("listbosses")){
				p.sendMessage(BossApi.getPlugin().bossNames.keySet().toString());
			}
		}
		
			
		else{
			sendHelp(p);
		}
	}
	
	@EventHandler
	public void OnBlockLeftClick(BlockBreakEvent e){
		if(e.getPlayer().equals(p)){
			if(state == RoomCreationState.EntryDoorRegion || state == RoomCreationState.ExitDoorRegion){
				Block b = e.getBlock();
				if(b != null){
					Location l = b.getLocation();
					currentDoorRegion.handleRegionSelectionA(new Vector3(l.getBlockX(), l.getBlockY(), l.getBlockZ()));
					sendMessage(p, "Set first corner to " + ChatColor.GOLD + currentDoorRegion.a);
				}
				e.setCancelled(true);
			}
			if(state == RoomCreationState.RedstoneSelection){
				if(e.getBlock().getType() == Material.REDSTONE_LAMP_OFF){
					((RedstoneRoom)currentRoom).powered_blocks.add(e.getBlock().getLocation());
					sendMessage(p, "Block was added at location, " + e.getBlock().getLocation().toVector().toString());
					sendMessage(p, "Use the command /room exit, to finish selecting powered blocks");
				}
				e.setCancelled(true);
			}
		}
		
	}
	
	@EventHandler
	public void OnBlockRightClick(PlayerInteractEvent e){
		if(e.getPlayer().equals(p) && e.getAction() == Action.RIGHT_CLICK_BLOCK){
			if(state == RoomCreationState.EntryDoorRegion || state == RoomCreationState.ExitDoorRegion){
				Block b = e.getClickedBlock();
				if(b != null){
					Location l = b.getLocation();
					currentDoorRegion.handleRegionSelectionB(new Vector3(l.getBlockX(), l.getBlockY(), l.getBlockZ()));
					sendMessage(p, "Set second corner to " + ChatColor.GOLD + currentDoorRegion.b);
				}
				
				e.setCancelled(true);
			}
		}
	}
	
	public void sendHelp(Player p){
		sendMessage(p, ChatColor.GOLD + "/room create <type> [insert pos] " + ChatColor.GRAY + "- creates a new room, with optional insert (types : Combat, Redstone)");
		sendMessage(p, ChatColor.GOLD + "/room delete <pos> " + ChatColor.GRAY + "- deletes the room in the specified position");
		sendMessage(p, ChatColor.GOLD + "/room detail <pos> " + ChatColor.GRAY + "- displays detailed information about the given room");
		sendMessage(p, ChatColor.GOLD + "/room exit " + ChatColor.GRAY + "- exits the room creator");
		sendMessage(p, ChatColor.GOLD + "/room list " + ChatColor.GRAY + "- lists out the rooms and their types");
		sendMessage(p, ChatColor.GOLD + "/room next " + ChatColor.GRAY + "- progresses the room creator to the next step");
		sendMessage(p, ChatColor.GOLD + "/room setspawn <mobtype> <count> " + ChatColor.GRAY + "- set a spawnpoint for a <count> number of monsters, of type <mobtype>");
		
	}
	
	public void sendMessage(Player p, String message){
		p.sendMessage(ChatColor.GOLD + "[Room]" + ChatColor.GRAY + ":" + message);
	}
	
	public boolean checkEntityName(String name){
		try{
			if(isBoss(name)){
				return true;
			}
			else{
				EntityType.valueOf(name.toUpperCase());
				return true;
			}
		}
		catch(IllegalArgumentException e){
			return false;
		}
	}
	
	public boolean isBoss(String name){
		if(BossApi.getPlugin().bossNames.keySet().contains(name)){
			return true;
		}
		return false;
	}
	
}
