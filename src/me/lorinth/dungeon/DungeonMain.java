package me.lorinth.dungeon;

import java.io.File;
import java.util.HashMap;

import me.lorinth.dungeon.objects.Dungeon;
import me.lorinth.dungeon.objects.Party;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class DungeonMain extends JavaPlugin implements Listener{

	public ConsoleCommandSender console;
	HashMap<String, Dungeon> dungeons = new HashMap<String, Dungeon>();
	public static DungeonMain instance;
	public static PartyManager pManager;
	
	HashMap<Player, Dungeon> playerCreators = new HashMap<Player, Dungeon>();
	public HashMap<Party, Dungeon> dungeonIsReady = new HashMap<Party, Dungeon>();
	public HashMap<Player, RoomEditor> roomEditors = new HashMap<Player, RoomEditor>();
	
	@Override
	public void onEnable(){
		console = Bukkit.getConsoleSender();
		instance = this;
		pManager = new PartyManager();
		console.sendMessage(ChatColor.DARK_RED + "[Dungeons]" + ChatColor.GRAY + " has been enabled");
		
		Bukkit.getPluginManager().registerEvents(this, this);
		
		loadDungeons();
	}
	
	@Override
	public void onDisable(){
		saveDungeons();
		console.sendMessage(ChatColor.DARK_RED + "[Dungeons]" + ChatColor.GRAY + " has been disabled");
	}
	
	public static DungeonMain getInstance(){
		return instance;
	}
	
	public static PartyManager getPartyManager(){
		return pManager;
	}
	
	public void saveDungeons(){
		for(Dungeon d : dungeons.values()){
			d.save();
		}
	}
	
	public void loadDungeons(){
		File directory = new File(getDataFolder().toString());
		if(directory.exists()){
			//LorinthsDungeons/
			for(File f : directory.listFiles()){
				if(f.getName().contains(".yml")){
					//The_Hollowed_Crypt.yml
					String name = f.getName().replace("_", " ");
					name = name.replace(".yml", " ");
					name = name.trim();
					//The Hollowed Crypt
					
					Dungeon dungeon = new Dungeon(this, name);
					dungeon.load();
					dungeons.put(name, dungeon);
				}
			}
		}
		
	}
	
	public void displayHelp(Player p){
		p.sendMessage(ChatColor.DARK_RED + "[Dungeon Commands]");
		p.sendMessage(ChatColor.GOLD + "/dungeon create <name>"+ ChatColor.GRAY +" - create the dungeon with a given name");
		p.sendMessage(ChatColor.GOLD + "/dungeon delete <name>"+ ChatColor.GRAY +" - removes the dungeon with a given name");
		p.sendMessage(ChatColor.GOLD + "/dungeon enable"+ ChatColor.GRAY +" - toggles the dungeon to be enabled for players to enter");
		p.sendMessage(ChatColor.GOLD + "/dungeon help"+ ChatColor.GRAY +" - display this list");
		p.sendMessage(ChatColor.GOLD + "/dungeon join" + ChatColor.GRAY + " - sends your party into the dungeon you are queued for (if it is ready)");
		p.sendMessage(ChatColor.GOLD + "/dungeon list"+ ChatColor.GRAY +" - lists all dungeons with an active/inactive tag");
		p.sendMessage(ChatColor.GOLD + "/dungeon load"+ ChatColor.GRAY +" - overloads all dungeons with changes from files");
		p.sendMessage(ChatColor.GOLD + "/dungeon lock"+ ChatColor.GRAY +" - will lock a dungeon so players cannot enter");
		p.sendMessage(ChatColor.GOLD + "/dungeon setstart [name]"+ ChatColor.GRAY +" - Sets the entrance warp when a dungeon has been started");
		p.sendMessage(ChatColor.GOLD + "/dungeon setexit [name]"+ ChatColor.GRAY +" - Sets the exit warp location when a dungeon is left");
		p.sendMessage(ChatColor.GOLD + "/dungeon setkey"+ ChatColor.GRAY +" - turn the held item into a required key (not neccesary for all dungeons)");
		p.sendMessage(ChatColor.GOLD + "/dungeon setreward <gold/exp> <amount> <name>" + ChatColor.GRAY + " - sets the gold or exp reward for a given dungeon");
		p.sendMessage(ChatColor.GOLD + "/room edit <name>"+ ChatColor.GRAY +" - allows you to edit rooms for a specific dungeon");
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args){
		// /dungeon <>
		if(sender instanceof Player){
			Player p = (Player) sender;
			if(commandLabel.equalsIgnoreCase("dungeon")){
				if(args.length == 0){
					displayHelp(p);
				}
				else{
					String command = args[0];
					if(command.equalsIgnoreCase("help")){
						displayHelp(p);
					}
					else if(command.equalsIgnoreCase("create")){
						if(args.length >= 2){
							String name = "";
							for(int i=1; i<args.length; i++){
								name += args[i] + " ";
							}
							name = name.trim();
							
							Dungeon d = new Dungeon(this, name);
							sendDungeonMessage(p, ChatColor.GRAY + "You have started creating the dungeon, " + name);
							sendDungeonMessage(p, ChatColor.GRAY + "Define a start point, and then type " + ChatColor.GOLD + "/dungeon setstart");
							this.dungeons.put(name, d);
							this.playerCreators.put(p, d);
							
						}
						else{
							
						}
					}
					else if(command.equalsIgnoreCase("delete")){
						//DELETE
					}
					else if(command.equalsIgnoreCase("setkey")){
						//SETKEY
					}
					else if(command.equalsIgnoreCase("join")){
						if(pManager.playerHasParty(p)){
							Party party = pManager.getPartyOfPlayer(p);
							if(this.dungeonIsReady.containsKey(party)){
								Dungeon d = dungeonIsReady.get(party);
								boolean success = d.start();
								if(success){
									dungeonIsReady.remove(party);
								}
								
							}
						}
						else{
							sendDungeonMessage(p, "You are not in a party!");
						}
					}
					else if(command.equalsIgnoreCase("enable")){
						String name = "";
						for(int i=1; i<args.length; i++){
							name += args[i] + " ";
						}
						name = name.trim();
						dungeons.get(name).isLocked = false;
						sendDungeonMessage(p, "You have enabled the dungeon, " + ChatColor.GOLD + name);
					}
					else if(command.equalsIgnoreCase("load")){
						String name = "";
						for(int i=1; i<args.length; i++){
							name += args[i] + " ";
						}
						name = name.trim();
						
						if(name.equalsIgnoreCase("all")){
							this.loadDungeons();
							sendDungeonMessage(p, "You have reloaded all of the dungeons!!!");
						}
						else{
							dungeons.get(name).load();
							sendDungeonMessage(p, "You have reloaded the dungeon, " + ChatColor.GOLD + name);
						}
						
						
					}
					else if(command.equalsIgnoreCase("lock")){
						String name = "";
						for(int i=1; i<args.length; i++){
							name += args[i] + " ";
						}
						name = name.trim();
						dungeons.get(name).isLocked = true;
						sendDungeonMessage(p, "You have locked the dungeon, " + ChatColor.GOLD + name);
					}
					
					//Creation commands
					else if(command.equalsIgnoreCase("setstart")){
						Dungeon d;
						if(args.length > 1){
							String name = "";
							for(int i=1; i<args.length; i++){
								name += args[i] + " ";
							}
							name = name.trim();
							d = this.dungeons.get(name);
						}
						else{
							d = playerCreators.get(p);
						}
						
						Location l = p.getLocation();
						d.start = l;
						sendDungeonMessage(p, "You have set the dungeon start point for, " + d.name);
						sendDungeonMessage(p, "Define an exit point, and then type " + ChatColor.GOLD + "/dungeon setexit");
					}
					else if(command.equalsIgnoreCase("setexit")){
						Dungeon d;
						
						if(args.length > 1){
							String name = "";
							for(int i=1; i<args.length; i++){
								name += args[i] + " ";
							}
							name = name.trim();
							d = this.dungeons.get(name);
						}
						else{
							d = playerCreators.get(p);
						}
						Location l = p.getLocation();
						d.exit = l;
						
						sendDungeonMessage(p, "To edit the rooms of the dungeon, use...");
						sendDungeonMessage(p, ChatColor.GOLD + "/room edit " + d.name);
					}
					else if(command.equalsIgnoreCase("setreward")){
						if(args.length < 4){
							p.sendMessage(ChatColor.GOLD + "/dungeon setreward <gold/exp> <amount> <name>" + ChatColor.GRAY + " - sets the gold or exp reward for a given dungeon");
						}
						String reward = args[1];
						int amount = Integer.parseInt(args[2]);
						String name = "";
						for(int i=3; i<args.length; i++){
							name += args[i] + " ";
						}
						name = name.trim();
						Dungeon d = this.dungeons.get(name);
						
						if(d != null){
							if(reward.equalsIgnoreCase("gold")){
								d.gold = amount;
								sendDungeonMessage(p, "You have set the gold reward of the dungeon, " + ChatColor.GOLD + name + ChatColor.GRAY + " to " + ChatColor.GOLD + amount);
							}
							else if(reward.equalsIgnoreCase("exp")){
								d.exp = amount;
								sendDungeonMessage(p, "You have set the exp reward of the dungeon, " + ChatColor.GOLD + name + ChatColor.GRAY + " to " + ChatColor.GOLD + amount);
							}
						}
						else{
							sendDungeonMessage(p, "The dungeon, " + name + ", was not found");
						}
					}
					else if(command.equalsIgnoreCase("list")){
						p.sendMessage(ChatColor.DARK_RED + "Dungeons");
						for(Dungeon d : this.dungeons.values()){
							String tag = "";
							if(d.isLocked){
								tag = ChatColor.RED + "inactive";
							}
							else{
								tag = ChatColor.GREEN + "active";
							}
							p.sendMessage(ChatColor.RED + d.name + ChatColor.GRAY + " [" + tag + ChatColor.GRAY + "]");
						}
					}
					else if(command.equalsIgnoreCase("start")){
						String name = "";
						for(int i=1; i<args.length; i++){
							name += args[i] + " ";
						}
						name = name.trim();
						Dungeon d = this.dungeons.get(name);
						
						if(d != null){
							Party party = null;
							if(pManager.playerHasParty(p)){
								party = pManager.getPartyOfPlayer(p);
							}
							else{
								pManager.player_parties.put(p, new Party(p));
								party = pManager.getPartyOfPlayer(p);
							}
							d.queueParty(party);
							
						}
						else{
							sendDungeonMessage(p, "The dungeon, " + name + ", was not found");
						}
					}
				}
			}
			else if(commandLabel.equalsIgnoreCase("room")){
				if(roomEditors.containsKey(p)){
					this.roomEditors.get(p).handleCommand(args);
				}
				else{
					if(args.length > 0){
						if(args[0].equalsIgnoreCase("edit")){
							String name = "";
							for(int i=1; i<args.length; i++){
								name += args[i] + " ";
							}
							name = name.trim();
							Dungeon d = this.dungeons.get(name);
							if(d != null){
								RoomEditor re = new RoomEditor(this, p, d);
								roomEditors.put(p, re);
							}
							else{
								sendDungeonMessage(p, "The dungeon, " + ChatColor.GOLD + name + ChatColor.GRAY + ", doesn't exist");
								System.out.println(dungeons.toString());
							}
						}
						else{
							p.sendMessage(ChatColor.GOLD + "[Room]" + ChatColor.GRAY + ": Commands");
							p.sendMessage(ChatColor.GOLD + "[Room]: /room edit <name> - edit the rooms in a dungeon");
						}
					}
					else{
						p.sendMessage(ChatColor.GOLD + "[Room]" + ChatColor.GRAY + ": Commands");
					}
				}
			}
			else if(commandLabel.equalsIgnoreCase("party")){
				pManager.handleCommand(p, args);
			}
		}
		else{
			//Console is worthless
		}
		
		return false;
	}
	
	public void sendDungeonMessage(Player p, String message){
		p.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ":" + message);
	}
	
}
