package me.lorinth.dungeon.objects;

import java.util.ArrayList;
import java.util.HashMap;

import me.Lorinth.BossApi.BossApi;
import me.lorinth.dungeon.DungeonMain;
import me.lorinth.dungeon.tasks.CombatRoomTask;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.Listener;

public class CombatRoom extends Room implements Listener{

	public HashMap<Location, ArrayList<String>> normal_spawns = new HashMap<Location, ArrayList<String>>();
	public HashMap<Location, ArrayList<String>> boss_spawns = new HashMap<Location, ArrayList<String>>();
	public ArrayList<Entity> mobs = new ArrayList<Entity>();
	
	public CombatRoom(Dungeon d){
		super(RoomType.Combat, d);
	}
	
	public void save(YamlConfiguration yml){
		yml.set("Combat", null); //Clear the list each time we save so we don't stack spawns each save
		
		
		for(Location loc : normal_spawns.keySet()){
			yml.set("Combat.Normal." + getLocationToString(loc), normal_spawns.get(loc));
		}
		
		for(Location loc : boss_spawns.keySet()){
			yml.set("Combat.Boss." + getLocationToString(loc), boss_spawns.get(loc));
		}
	}
	
	private String getLocationToString(Location l){
		String x = l.getX() + "";
		x = x.replace(".", "/");
		String y = l.getY() + "";
		y = y.replace(".", "/");
		String z = l.getZ() + "";
		z = z.replace(".", "/");
		String yaw = l.getYaw() + "";
		yaw = yaw.replace(".", "/");
		String pitch = l.getPitch() + "";
		pitch = pitch.replace(".", "/");
		
		return "<" + l.getWorld().getName() + "," + x + "," + y + "," + z + "," + yaw + "," + pitch  + ">";
	}
	
	public void load(YamlConfiguration yml){
		super.load(yml);
		
		try{
			for(String s : yml.getConfigurationSection("Combat.Normal").getKeys(false)){
				Location l = getLocationFromString(s);
				normal_spawns.put(l, (ArrayList<String>) yml.getStringList("Combat.Normal." + s));
			}
		}
		catch(NullPointerException e){
			//No normal spawns
		}
		try{
			for(String s : yml.getConfigurationSection("Combat.Boss").getKeys(false)){
				Location l = getLocationFromString(s);
				boss_spawns.put(l, (ArrayList<String>) yml.getStringList("Combat.Boss." + s));
			}
		}
		catch(NullPointerException e){
			//No boss spawns
		}
	}
	
	private Location getLocationFromString(String s){
		s = s.replace("<", "").replace(">", "");
		String[] args = s.split(",");
		String x = args[1].replace("/", ".");
		String y = args[2].replace("/", ".");
		String z = args[3].replace("/", ".");
		String yaw = args[4].replace("/", ".");
		String pitch = args[5].replace("/", ".");
		return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(x), Double.parseDouble(y), Double.parseDouble(z), Float.parseFloat(yaw), Float.parseFloat(pitch));
	}

	@Override
	public void start() {
		for(Location l : normal_spawns.keySet()){
			for(String s : normal_spawns.get(l)){
				mobs.add(l.getWorld().spawnEntity(l, EntityType.valueOf(s)));
			}
		}
		for(Location l : boss_spawns.keySet()){
			for(String s : boss_spawns.get(l)){
				mobs.add(BossApi.getPlugin().bossNames.get(s).spawn(l).getBossEntity());
			}
		}
		
		CombatRoomTask task = new CombatRoomTask(this);
		task.runTaskTimer(DungeonMain.getInstance(), 20, 20);
		super.start();
	}
	
}
