package me.lorinth.dungeon.objects;

import java.util.ArrayList;

import me.lorinth.dungeon.DungeonMain;
import me.lorinth.dungeon.tasks.RedstoneRoomTask;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;

public class RedstoneRoom extends Room{

	public ArrayList<Location> powered_blocks = new ArrayList<Location>();
	
	public RedstoneRoom(Dungeon d){
		super(RoomType.Redstone, d);
	}

	@Override
	public void start() {
		RedstoneRoomTask task = new RedstoneRoomTask(this);
		task.runTaskTimer(DungeonMain.instance, 5, 5);
		super.start();
	}
	
	public void save(YamlConfiguration yml){
		int id = 0;
		for(Location l : powered_blocks){
			yml.set("Redstone." + id + ".world", l.getWorld().getName());
			yml.set("Redstone." + id + ".x", l.getBlockX());
			yml.set("Redstone." + id + ".y", l.getBlockY());
			yml.set("Redstone." + id + ".z", l.getBlockZ());
		}
	}
	
	public void load(YamlConfiguration yml){
		super.load(yml);
		
		for(String id : yml.getConfigurationSection("Redstone").getKeys(false)){
			Location loc = new Location(
					Bukkit.getWorld(yml.getString("Redstone." + id + ".world")),
					yml.getInt("Redstone." + id + ".x"),
					yml.getInt("Redstone." + id + ".y"),
					yml.getInt("Redstone." + id + ".z"));
			powered_blocks.add(loc);
		}
	}
}
