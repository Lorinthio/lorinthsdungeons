package me.lorinth.dungeon.objects;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import me.lorinth.dungeon.util.BlockData;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.YamlConfiguration;

public class Room {

	public enum RoomType{
		Combat, Redstone, Puzzle
	}
	
	public enum DoorState{
		Open, Closed
	}
	
	Dungeon dungeon;
	public RoomType type; //DONE
	
	
	public HashMap<DoorState, HashMap<Location, BlockData>> entrance; //Contains data on how to set specific blocks between open/closing
	public HashMap<DoorState, HashMap<Location, BlockData>> exit; //
	
	//Animations
	public boolean hasLoopAnimation = false;
	public HashMap<Integer, HashMap<Location, BlockData>> loopAnimation;
	public long loopFrameDuration = 5; // 0.25 seconds
	public boolean hasDoorAnimation = false; //only plays on the entrance door
	public HashMap<Integer, HashMap<Location, BlockData>> doorAnimation;
	public long doorFrameDuration = 5; //  0.25 seconds
	
	public void save(String path, Integer position){
		File f = new File(path + File.separator + "Room" + position + ".yml");
		YamlConfiguration yml = YamlConfiguration.loadConfiguration(f);
		yml.set("Type", type.toString());
		
		String prefix;
		Integer index;
		
		//Entrance.Open.0.Location
		if(entrance != null){
			if(entrance.containsKey(DoorState.Open)){
				prefix = "Entrance.Open.";
				index = 0;
				HashMap<Location, BlockData> ent_open = entrance.get(DoorState.Open);
				for(Location l : ent_open.keySet()){
					String localpre = prefix + index + ".Location.";
				
					yml.set(localpre + "world", l.getWorld().getName());
					yml.set(localpre + "x", l.getBlockX());
					yml.set(localpre + "y", l.getBlockY());
					yml.set(localpre + "z", l.getBlockZ());
					
					BlockData bd = ent_open.get(l);
					localpre = prefix + index +  ".BlockData.";
					yml.set(localpre + "material", bd.material.toString());
					yml.set(localpre + "data", bd.data);
					index += 1;
				}
			}
			
			if(entrance.containsKey(DoorState.Closed)){
				prefix = "Entrance.Closed.";
				index = 0;
				HashMap<Location, BlockData> ent_closed = entrance.get(DoorState.Closed);//
				for(Location l : ent_closed.keySet()){//
					String localpre = prefix + index + ".Location.";
				
					yml.set(localpre + "world", l.getWorld().getName());
					yml.set(localpre + "x", l.getBlockX());
					yml.set(localpre + "y", l.getBlockY());
					yml.set(localpre + "z", l.getBlockZ());
					
					BlockData bd = ent_closed.get(l);//
					localpre = prefix + index + ".BlockData.";
					yml.set(localpre + "material", bd.material.toString());
					yml.set(localpre + "data", bd.data);
					index += 1;
				}
			}
		}
		if(exit != null){
			if(exit.containsKey(DoorState.Open)){
				prefix = "Exit.Open.";
				index = 0;
				HashMap<Location, BlockData> exit_opened = exit.get(DoorState.Open);//
				for(Location l : exit_opened.keySet()){//
					String localpre = prefix + index + ".Location.";
				
					yml.set(localpre + "world", l.getWorld().getName());
					yml.set(localpre + "x", l.getBlockX());
					yml.set(localpre + "y", l.getBlockY());
					yml.set(localpre + "z", l.getBlockZ());
					
					BlockData bd = exit_opened.get(l);//
					localpre = prefix + index + ".BlockData.";
					yml.set(localpre + "material", bd.material.toString());
					yml.set(localpre + "data", bd.data);
					index += 1;
				}
			}
			if(exit.containsKey(DoorState.Closed)){
				prefix = "Exit.Closed.";
				index = 0;
				HashMap<Location, BlockData> exit_closed = exit.get(DoorState.Closed);//
				for(Location l : exit_closed.keySet()){//
					String localpre = prefix + index + ".Location.";
				
					yml.set(localpre + "world", l.getWorld().getName());
					yml.set(localpre + "x", l.getBlockX());
					yml.set(localpre + "y", l.getBlockY());
					yml.set(localpre + "z", l.getBlockZ());
					
					BlockData bd = exit_closed.get(l);//
					localpre = prefix + index + ".BlockData.";
					yml.set(localpre + "material", bd.material.toString());
					yml.set(localpre + "data", bd.data);
					index += 1;
				}
			}
		}

		if(this instanceof CombatRoom){
			CombatRoom cRoom = (CombatRoom) this;
			cRoom.save(yml);
		}
		if(this instanceof RedstoneRoom){
			RedstoneRoom rRoom = (RedstoneRoom) this;
			rRoom.save(yml);
		}
		try {
			yml.save(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void load(YamlConfiguration yml){
		type = RoomType.valueOf(yml.getString("Type"));
		
		//Entrance.Open.0.Location
		entrance = new HashMap<DoorState, HashMap<Location, BlockData>>();
		exit = new HashMap<DoorState, HashMap<Location, BlockData>>();
		String prefix;
		if(yml.contains("Entrance.Open")){
			System.out.println(type + " has no Entrance.Open");
			prefix = "Entrance.Open.";//
			HashMap<Location, BlockData> ent_open = new HashMap<Location, BlockData>();//
			for(String s : yml.getConfigurationSection("Entrance.Open").getKeys(false)){//
				String localpre = prefix + s + ".Location.";
				
				String world;
				int x, y, z;
				
				world = yml.getString(localpre + "world");
				x= yml.getInt(localpre + "x");
				y= yml.getInt(localpre + "y");
				z= yml.getInt(localpre + "z");
				
				Location loc = new Location(Bukkit.getWorld(world), x, y, z);
				
				Material material;
				byte data;
				
				localpre = prefix + s + ".BlockData.";
				material = Material.valueOf(yml.getString(localpre + "material"));
				data = ((Integer) yml.getInt(localpre + "data")).byteValue();
				BlockData bd = new BlockData(material, data);
				
				ent_open.put(loc, bd);
			}
			entrance.put(DoorState.Open, ent_open);
		}
		if(yml.contains("Entrance.Closed")){
			System.out.println(type + " has no Entrance.Closed");
			prefix = "Entrance.Closed.";//
			HashMap<Location, BlockData> ent_closed = new HashMap<Location, BlockData>();//
			for(String s : yml.getConfigurationSection("Entrance.Closed").getKeys(false)){//
				String localpre = prefix + s + ".Location.";
				
				String world;
				int x, y, z;
				
				world = yml.getString(localpre + "world");
				x= yml.getInt(localpre + "x");
				y= yml.getInt(localpre + "y");
				z= yml.getInt(localpre + "z");
				
				Location loc = new Location(Bukkit.getWorld(world), x, y, z);
				
				Material material;
				byte data;
				
				localpre = prefix + s + ".BlockData.";
				material = Material.valueOf(yml.getString(localpre + "material"));
				data = ((Integer) yml.getInt(localpre + "data")).byteValue();
				BlockData bd = new BlockData(material, data);
				
				ent_closed.put(loc, bd);
			}
			entrance.put(DoorState.Closed, ent_closed);
		}
		if(yml.contains("Exit.Open")){
			System.out.println(type + " has no Exit.Open");
			prefix = "Exit.Open.";//
			HashMap<Location, BlockData> exit_open = new HashMap<Location, BlockData>();//
			for(String s : yml.getConfigurationSection("Exit.Open").getKeys(false)){//
				String localpre = prefix + s + ".Location.";
				
				String world;
				int x, y, z;
				
				world = yml.getString(localpre + "world");
				x= yml.getInt(localpre + "x");
				y= yml.getInt(localpre + "y");
				z= yml.getInt(localpre + "z");
				
				Location loc = new Location(Bukkit.getWorld(world), x, y, z);
				
				Material material;
				byte data;
				
				localpre = prefix + s + ".BlockData.";
				material = Material.valueOf(yml.getString(localpre + "material"));
				data = ((Integer) yml.getInt(localpre + "data")).byteValue();
				BlockData bd = new BlockData(material, data);
				
				exit_open.put(loc, bd);
			}
			exit.put(DoorState.Open, exit_open);
		}
		if(yml.contains("Exit.Closed")){
			prefix = "Exit.Closed.";//
			System.out.println(type + " has no Exit.Closed");
			HashMap<Location, BlockData> exit_closed = new HashMap<Location, BlockData>();//
			for(String s : yml.getConfigurationSection("Exit.Closed").getKeys(false)){//
				String localpre = prefix + s + ".Location.";
				
				String world;
				int x, y, z;
				
				world = yml.getString(localpre + "world");
				x= yml.getInt(localpre + "x");
				y= yml.getInt(localpre + "y");
				z= yml.getInt(localpre + "z");
				
				Location loc = new Location(Bukkit.getWorld(world), x, y, z);
				
				Material material;
				byte data;
				
				localpre = prefix + s + ".BlockData.";
				material = Material.valueOf(yml.getString(localpre + "material"));
				data = ((Integer) yml.getInt(localpre + "data")).byteValue();
				BlockData bd = new BlockData(material, data);
				
				exit_closed.put(loc, bd);
			}
			exit.put(DoorState.Closed, exit_closed);
		}
	}
	
	public Dungeon getDungeon(){
		return this.dungeon;
	}
	
	public Room(Dungeon d){
		//USED FOR LOADING
		this.dungeon = d;
	}
	
	public Room(String type, Dungeon d){
		this.type = RoomType.valueOf(type);
		this.dungeon = d;
	}
	
	public Room(RoomType type, Dungeon d){
		this.type = type;
		this.dungeon = d;
	}
	
	//Helper function makes sure all doors are closed for a room
	@SuppressWarnings("deprecation")
	public void shutDoors(){
		if(entrance != null){
			HashMap<Location, BlockData> blocks = entrance.get(DoorState.Closed);
			if(blocks != null){
				for(Location l : blocks.keySet()){
					BlockData bd = blocks.get(l);
					Block b = l.getBlock();
					b.setType(bd.material);
					b.setData(bd.data);
				}
			}
		}

		//Exit is closed
		if(exit != null){
			HashMap<Location, BlockData> blocks = exit.get(DoorState.Closed);
			if(blocks != null){
				for(Location l : blocks.keySet()){
					BlockData bd = blocks.get(l);
					Block b = l.getBlock();
					b.setType(bd.material);
					b.setData(bd.data);
				}
			}
		}
		
	}
	
	//Called when the room has started and all players are inside
	@SuppressWarnings("deprecation")
	public void start(){
		System.out.println(type + " was started");
		if(this.hasDoorAnimation){
			
		}
		if(this.hasLoopAnimation){
			
		}
		
		//Entrance is open
		if(entrance != null){
			HashMap<Location, BlockData> blocks = entrance.get(DoorState.Open);
			if(blocks != null){
				for(Location l : blocks.keySet()){
					BlockData bd = blocks.get(l);
					Block b = l.getBlock();
					b.setType(bd.material);
					b.setData(bd.data);
				}
			}
			else{
				System.out.println("blocks is null");
			}
		}
		else{
			System.out.println("entrance is null");
		}
	}
	
	//Called when the room has finished
	public void complete(){
		if(exit != null){
			HashMap<Location, BlockData> blocks = exit.get(DoorState.Open);
			if(blocks != null){
				for(Location l : blocks.keySet()){
					BlockData bd = blocks.get(l);
					Block b = l.getBlock();
					b.setType(bd.material);
					b.setData(bd.data);
				}
			}
		}
		
		dungeon.startNextRoom();
	}
	
}
