package me.lorinth.dungeon.objects;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Party {

	Player leader;
	ArrayList<Player> members = new ArrayList<Player>();
	Integer maxPartySize = 4;
	
	public Party(Player leader){
		this.leader = leader;
		this.members.add(leader);
		sendPartyMessage(leader, "You have created a party!");
	}
	
	public Player getLeader(){
		return leader;
	}
	
	public void addPlayer(Player p){
		if(members.size() < maxPartySize){
			for(Player p2 : members){
				sendPartyMessage(p2, p.getDisplayName() + " has joined the party!");
			}
			members.add(p);
			sendPartyMessage(p, p.getDisplayName() + "You have joined, " + leader.getDisplayName() + "'s party!");
		}
		else{
			sendPartyMessage(p, ChatColor.RED + "The party is full!");
		}
		
	}
	
	public void removePlayer(Player p){
		if(members.size() >= 2){
			changeLeader(members.get(1));
		}
		sendPartyMessage(p, ChatColor.RED + "You have left the party!");
		members.remove(p);
		if(members.size() > 0){
			for(Player p2 : members){
				sendPartyMessage(p2, ChatColor.GOLD + p.getDisplayName() + ChatColor.RED + " has left the party!");			
			}
		}
	}
	
	public void changeLeader(Player p){
		sendPartyMessage(leader, ChatColor.RED + "You are no longer the party leader");
		leader = p;
		sendPartyMessage(p, ChatColor.GOLD + "You are the new party leader!!!");
		
		for(Player p2 : members){
			if(p2 != leader){
				sendPartyMessage(p2, ChatColor.GOLD + leader.getDisplayName() + " " + ChatColor.DARK_AQUA + "is the new party leader!");
			}
		}
	}
	
	public boolean isLeader(Player p){
		return leader.equals(p);
	}
	
	public boolean isMember(Player p){
		return members.contains(p);
	}
	
	public void disband(){
		for(Player p : members){
			sendPartyMessage(p, ChatColor.RED + "The party has been disbanded!");
		}
	}
	
	public void sendPartyMessage(Player p, String m){
		p.sendMessage(ChatColor.DARK_AQUA + "[" + ChatColor.GOLD + "Party" + ChatColor.DARK_AQUA + "]:" + ChatColor.GOLD + m);
	}
	
}
