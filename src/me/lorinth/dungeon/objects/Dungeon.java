package me.lorinth.dungeon.objects;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import me.lorinth.dungeon.DungeonMain;
import me.lorinth.dungeon.objects.Room.RoomType;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class Dungeon {

	public String name; //saved in the file name
	public Integer currentRoomPosition = 0; //not saved
	public ArrayList<Room> rooms = new ArrayList<Room>(); //saved seperately
	public ArrayList<Party> queue = new ArrayList<Party>(); //Not saved
	public Party currentParty = null; //Not saved
	public int exp = 0; //saved
	public int gold = 0; //saved
	
	public Integer LevelRequired = 1; //saved
	public Integer MembersRequired = 1; //saved
	
	public Location start; //for when players enter the dungeon
	public Location exit; //for when players complete the dungeon
	
	public boolean isLocked = true; //Is the dungeon open to the public/ready?   //saved
	
	DungeonMain main;
	File file;
	YamlConfiguration yaml;
	
	public Dungeon(){
		//Shouldn't ever be called
		this.name = "Untitled Dungeon";
	}
	
	public Dungeon(DungeonMain main, String name){
		this.main = main;
		this.name = name;
	}
	
	public void queueParty(Party p){
		queue.add(p);
		
		if(currentParty == null){
			currentParty = p;
			queue.remove(p);
			
			DungeonMain.getInstance().dungeonIsReady.put(currentParty, this);
			currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": Your dungeon, " + ChatColor.GOLD + this.name + ChatColor.GRAY + " is ready to enter!");
			currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": Use the command, " + ChatColor.GOLD + "/dungeon join" + ChatColor.GRAY + ", when your party is ready!");
		}
	}
	
	//TODO
	public void save(){
		try {
			String local_name = name.replace(" ", "_");
			
			file = new File(DungeonMain.getInstance().getDataFolder(), local_name + ".yml");
			yaml = YamlConfiguration.loadConfiguration(file);
			yaml.set("Locked", this.isLocked);
			yaml.set("Rewards.Exp", this.exp);
			yaml.set("Rewards.Gold", this.gold);
			yaml.set("Requirements.Level", this.LevelRequired);
			yaml.set("Requirements.PartySize", this.MembersRequired);
			//Positions
			yaml.set("Position.Start.world", this.start.getWorld().getName());
			yaml.set("Position.Start.x", this.start.getX());
			yaml.set("Position.Start.y", this.start.getY());
			yaml.set("Position.Start.z", this.start.getZ());
			yaml.set("Position.Start.pitch", this.start.getPitch());
			yaml.set("Position.Start.yaw", this.start.getYaw());
			
			yaml.set("Position.Exit.world", this.exit.getWorld().getName());
			yaml.set("Position.Exit.x", this.exit.getX());
			yaml.set("Position.Exit.y", this.exit.getY());
			yaml.set("Position.Exit.z", this.exit.getZ());
			yaml.set("Position.Exit.pitch", this.exit.getPitch());
			yaml.set("Position.Exit.yaw", this.exit.getYaw());
			
			File directory = new File(DungeonMain.getInstance().getDataFolder().getAbsolutePath() + file.separator + local_name + file.separator + "Rooms");
			if(!directory.exists()){
				directory.mkdirs();
			}
			Integer roomCount = 1;
			for(Room r : this.rooms){
				r.save(directory.getAbsolutePath(), roomCount);
				roomCount += 1;
			}
			
			
			yaml.save(file);
		} catch (IOException | NullPointerException e) {
			e.printStackTrace();
			main.console.sendMessage(ChatColor.DARK_RED + "[Dungeons] : Did not save the dungeon, " + name + ", correctly!!!");
		}
	}
	
	public void load(){
		String local_name = name.replace(" ", "_");
		
		file = new File(main.getDataFolder(), local_name + ".yml");
		yaml = YamlConfiguration.loadConfiguration(file);
		this.isLocked = yaml.getBoolean("Locked");
		this.exp = yaml.getInt("Rewards.Exp");
		this.gold = yaml.getInt("Rewards.Gold");
		this.LevelRequired = yaml.getInt("Requirements.Level");
		this.MembersRequired = yaml.getInt("Requirements.PartySize");
		
		//temp variables for positions
		World world;
		double x, y, z;
		float yaw, pitch;
		//Load Start Point
		world = Bukkit.getWorld(yaml.getString("Position.Start.world"));
		x = yaml.getDouble("Position.Start.x");
		y = yaml.getDouble("Position.Start.y");
		z = yaml.getDouble("Position.Start.z");
		yaw = (float) yaml.getDouble("Position.Start.yaw");
		pitch = (float) yaml.getDouble("Position.Start.pitch");
		this.start = new Location(world, x, y, z, yaw, pitch);
		//Load Exit Point
		world = Bukkit.getWorld(yaml.getString("Position.Exit.world"));
		x = yaml.getDouble("Position.Exit.x");
		y = yaml.getDouble("Position.Exit.y");
		z = yaml.getDouble("Position.Exit.z");
		yaw = (float) yaml.getDouble("Position.Exit.yaw");
		pitch = (float) yaml.getDouble("Position.Exit.pitch");
		this.exit = new Location(world, x, y, z, yaw, pitch);
		
		//Start Room Loading
		File directory = new File(DungeonMain.getInstance().getDataFolder().getAbsolutePath() + file.separator + local_name + file.separator + "Rooms");
		if(directory.exists()){
			//load in rooms
			for(File f : directory.listFiles()){
				YamlConfiguration yml = YamlConfiguration.loadConfiguration(f);
				RoomType type = RoomType.valueOf(yml.getString("Type"));
				Room r = null;
				if(type == RoomType.Combat){
					r = new CombatRoom(this);
					
				}
				else if(type == RoomType.Redstone){
					r = new RedstoneRoom(this);
				}
				r.load(yml);
				rooms.add(r);
			}
		}
		
	}
	
	public boolean start(){
		if(currentParty.members.size() < this.MembersRequired){
			currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": You do not have enough members in your party, Required: " + this.MembersRequired);
			return false;
		}
		
		boolean underlevel = false;
		
		for(Player p : currentParty.members){
			if(p.getLevel() < this.LevelRequired){
				currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": Your party member, " + p.getDisplayName() + ", is not a high enough level. (" + p.getLevel() + " / " + this.LevelRequired + ")");
				underlevel = true;
			}
		}
		
		if(underlevel){
			return false;
		}
		
		long delay = 0;
		for(final Room r : this.rooms){
			Bukkit.getScheduler().scheduleSyncDelayedTask(DungeonMain.getInstance(), new Runnable(){

				@Override
				public void run() {
					r.shutDoors();
				}
				
			}, delay);
			delay += 10;
		}
		
		for(Player p : currentParty.members){
			p.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": You are going to teleport to... " + ChatColor.GOLD + this.name + ChatColor.GRAY + " in 5 seconds!");
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(DungeonMain.getInstance(), new Runnable(){

			@Override
			public void run() {
				for(Player p : currentParty.members){
					p.teleport(start);
				}
			}
			
		}, 20 * 5);
		Bukkit.getScheduler().scheduleSyncDelayedTask(DungeonMain.getInstance(), new Runnable(){

			@Override
			public void run() {
				currentRoomPosition = -1;
				startNextRoom();
			}
			
		}, 20 * 7);
		
		
		return true;
	}
	
	public void stop(){
		for(Player p : currentParty.members){
			p.teleport(this.exit);
			p.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": The dungeon has collapsed...");
			
			//TODO : Give players money
			//Vault for economy
		}
	}
	
	//Starts room at a given position
	public void startRoom(int index){
		try{
			Room r = this.rooms.get(index);
			r.start();
		}
		catch(IndexOutOfBoundsException e){
			end();
		}
	}
	
	//Used in the vanilla dungeon sequence, concurrently calls the next room to start
	public void startNextRoom(){
		currentRoomPosition += 1;
		startRoom(currentRoomPosition);
	}
	
	public void end(){
		for(Player p : currentParty.members){
			p.giveExp(exp);
			p.teleport(this.exit);
			p.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": You have completed the dungeon, " + ChatColor.GOLD + this.name);
			
			//TODO : Give players money
			//Vault for economy
		}
		
		currentParty = null;
		queue.remove(currentParty);
		if(queue.size() != 0){
			currentParty = queue.get(0);
			queue.remove(currentParty);
			DungeonMain.getInstance().dungeonIsReady.put(currentParty, this);
			currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": Your dungeon, " + ChatColor.GOLD + this.name + ChatColor.GRAY + " is ready to enter!");
			currentParty.leader.sendMessage(ChatColor.DARK_RED + "[Dungeon]" + ChatColor.GRAY + ": Use the command, " + ChatColor.GOLD + "/dungeon join" + ChatColor.GRAY + ", when your party is ready!");
		}
	}
	
}
